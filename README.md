# Black Soul Gems

OpenMW Dev build(0.49) required

Once installed, this mod will add a necromancer's alter to the velothi tower Mawia, south of Molag Mar.

A Necromancer lives there. Kill him, and his minions, and you will be able to use the tower for your own necromancy of sorts.


This mod takes inspiration from Oblivion Black soul gems, you can transform Grand Soul Gems to Black Soul Gems.

You can only do this on the Shade of the Revenant, which happens every 8 days, starting with day 8. Check the "Day X" on your rest menu. If the number is a multiple of 8, it is the Shade of the Revenant.

Walk up to the alter, and place grand soul gems in the alter and cast "soul trap" on it (on the right day). It will transform 1 soul gem per cast. Make sure you have the "soul trap" spell, not a custom or other spell.

Now that you have the black soul gems, you can cast soul trap on most unique NPCs in the vanilla GOTY, including people like Fargoth and Gaenor. 


The NPCs you can trap include all NPCs in the base game and expansions, as well as Tamriel Rebuilt(as of this writing), and any NPCs added by "Beautiful Cities of Morrowind"

You can use normal spells, enchantments, or weapons to cast soul trap on people.

Black soul gems can only be used for black souls ( human souls), not animals or daedra. All people have the same soul size, 400, the same as a golden saint. This may not be balanced, but this could change in the future.

If you have OAAB_Data installed, the black soul gem from that will be used instead.

There is a second ESP included, Black Soul Gems - Message Fix
This fixes the GMST that normally says "Your have trapped a soul". If you have the patch for purists or some other fan patch, it's not needed.

If you don't use OpenMW, you can use Seph's NPC Soul Trapping, likely a much better mod [here](https://www.nexusmods.com/morrowind/mods/50744)

#### Credits

Author: ZackHasaCat

#### Installation

1. Download the mod from [this URL](https://gitlab.com/api/v4/projects/zackhasacat%2FblackSoulGems/jobs/artifacts/master/raw/BlackSoulGems.zip?job=make)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\black-soul-gems

        # Linux
        /home/username/games/OpenMWMods/black-soul-gems

        # macOS
        /Users/username/games/OpenMWMods/black-soul-gems

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\black-soul-gems"`)
1. Add `content=blacksoulgems.esp` and `content=blacksoulgems.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/black-soul-gems/-/issues)
* Email `zack@iwsao.com`
* Contact the author on Discord: `@ZackHasaCat`
